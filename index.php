<html>
<head>
    <link href="style.css" rel="stylesheet" type="text/css"/>
    <title>Dragons of Mugloar Solution</title>
    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
</head>
<body>
<div class="gameData">
    <p>Press the button to fetch game data and then press "solve battle" button to magically create your dragon to fight
        knight</p>
    <input type="button" class="startGame" value="Start Game">
    <div class="gameDetails">
    </div>
    <div class="dragon">
    </div>
    <input type="button" class="solveBattle" style="display: none;" value="Solve Battle">
    <div class="gameResult">

    </div>
</div>
<div class="fightData">
    <p>Fight Details</p>
    <p class="total">Total Fights : <span></span></p>
    <p class="win">Wins : <span></span></p>
    <p class="lose">Loses : <span></span></p>
    <p class="storm">Storm Fights : <span></span></p>
</div>
<script>
    var gameId, weather, knight, totalFights = 0, wins = 0, winPer = 0, loses = 0, losesPer = 0, stormFights = 0,
        stormFightsPer = 0;
    $('.startGame').click(function () {
        $.ajax({
            type: "GET",
            url: "functions.php",
            data: {
                func: "getGameData"
            }
        }).done(function (res) {
            var obj = JSON.parse(res), sum = 0;
            console.log(obj);
            if (obj.data) {
                $('.gameResult').html('');

                var html1 = '<ul>' +
                    '<li>Game ID : ' + obj.data.gameId + '</li>' +
                    '<li>Knight' +
                    '<ul>' +
                    '<li>Name : ' + obj.data.knight.name + '</li>' +
                    '<li>Attack : ' + obj.data.knight.attack + '</li>' +
                    '<li>Armor : ' + obj.data.knight.armor + '</li>' +
                    '<li>Agility : ' + obj.data.knight.agility + '</li>' +
                    '<li>Endurance : ' + obj.data.knight.endurance + '</li>' +
                    '</ul>' +
                    '</li>' +
                    '<li>Weather ' +
                    '<ul>' +
                    '<li>Time : ' + obj.weather.time + '</li>' +
                    '<li>Coordinates ' +
                    '<ul>' +
                    '<li>X : ' + obj.weather.coords.x + '</li>' +
                    '<li>Y : ' + obj.weather.coords.y + '</li>' +
                    '<li>Z : ' + obj.weather.coords.z + '</li>' +
                    '</ul>' +
                    '</li>' +
                    '<li>Code : ' + obj.weather.code + '</li>' +
                    '<li>Message : ' + obj.weather.message + '</li>' +
                    '</ul>' +
                    '</li>' +
                    '</ul>' +
                    '</ul>';
                gameId = obj.data.gameId;
                knight = obj.data.knight;
                weather = obj.weather;
                $('.gameDetails').html(html1);
                $('.solveBattle').show();
            }
        });
    });

    $('.solveBattle').click(function () {
        $.ajax({
            type: "GET",
            url: "functions.php",
            data: {
                func: "solveBattle", gameId: gameId, knight: knight, weather: weather, totalFights: totalFights,
                wins: wins, winPer: winPer, loses: loses, losesPer: losesPer, stormFights: stormFights,
                stormFightsPer: stormFightsPer
            }
        }).done(function (res) {
            var obj = JSON.parse(res);
            console.log(obj.data);
            if (obj.data) {
                var html1 = '<ul>The mighty dragon sent to fight the knight' +
                    '<li>Scale Thickness  ' + obj.dragon.scaleThickness + '</li>' +
                    '<li>Claw Sharpness  ' + obj.dragon.clawSharpness + '</li>' +
                    '<li>Wing Strength  ' + obj.dragon.wingStrength + '</li>' +
                    '<li>Fire Breath  ' + obj.dragon.fireBreath + '</li>' +
                    '</ul>' +
                    '<div>Status : ' + obj.data.status + '</div>' +
                    '<div>Message : ' + obj.data.message + '</div>';
                $('.gameResult').html(html1);
                totalFights = obj.fightData.totalFights;
                wins = obj.fightData.wins;
                winPer = obj.fightData.winPer;
                loses = obj.fightData.loses;
                losesPer = obj.fightData.losesPer;
                stormFights = obj.fightData.stormFights;
                stormFightsPer = obj.fightData.stormFightsPer;
                $('.fightData .total span').text(obj.fightData.totalFights);
                $('.fightData .win span').text(obj.fightData.wins + " (" + obj.fightData.winPer + "%)");
                $('.fightData .lose span').text(obj.fightData.loses + " (" + obj.fightData.losesPer + "%)");
                $('.fightData .storm span').text(obj.fightData.stormFights + " (" + obj.fightData.stormFightsPer + "%)");
            }
        });
    });
</script>

</body>
</html>

