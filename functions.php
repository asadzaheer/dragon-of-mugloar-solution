<?php
//print_r($_GET);
//print_r($_POST);
//die;
$fun    = $knight = $weather = '';
$gameId = 0;
if (!isset($_POST['$totalFights']) || !isset($wins) || !isset($loses) || !isset($winPer) || !isset($stormFights) || !isset($stormFightsPer)) {
    $totalFights = $wins = $loses = $winPer = $stormFights = $stormFightsPer = 0;
}
if (!empty($_GET) && isset($_GET['func'])) {
    $fun = $_GET['func'];
    if ($fun === 'solveBattle') {
        $gameId = (isset($_GET['gameId'])) ? $_GET['gameId'] : 0;
        if (!$gameId) {
            echo json_encode(['msg' => 'Invalid or no game id provided.']);
            die;
        }
        $knight         = (isset($_GET['knight'])) ? $_GET['knight'] : '';
        $weather        = (isset($_GET['weather'])) ? $_GET['weather'] : '';
        $totalFights    = (isset($_GET['totalFights'])) ? $_GET['totalFights'] : 0;
        $wins           = (isset($_GET['wins'])) ? $_GET['wins'] : 0;
        $loses          = (isset($_GET['loses'])) ? $_GET['loses'] : 0;
        $winPer         = (isset($_GET['winPer'])) ? $_GET['winPer'] : 0;
        $stormFights    = (isset($_GET['stormFights'])) ? $_GET['stormFights'] : 0;
        $stormFightsPer = (isset($_GET['stormFightsPer'])) ? $_GET['stormFightsPer'] : 0;
    }
}
//else if (!empty($_POST) && isset($_POST['func'])) {
//    $fun = $_POST['func'];
//    if ($fun === 'solveBattle') {
//        $gameId = (isset($_POST['gameId'])) ? $_POST['gameId'] : 0;
//        if (!$gameId) {
//            echo json_encode(['msg' => 'Invalid or no game id provided.']);
//            die;
//        }
//        $knight  = (isset($_GET['knight'])) ? $_GET['knight'] : '';
//        $weather = (isset($_GET['weather'])) ? $_GET['weather'] : '';
//    }
//}
else {
    echo json_encode(['msg' => 'No data provided']);
    die;
}
switch ($fun) {
    case 'getGameData':
        getGameData();
        break;
    case 'solveBattle':
        solveBattle($gameId, $knight, $weather);
        break;
    default:
        echo json_encode(['msg' => 'invalid function']);
        die;
}

function getGameData() {
    //setup the request, you can also use CURLOPT_URL
    $ch = curl_init('http://www.dragonsofmugloar.com/api/game');

    // Returns the data/output as a string instead of raw data
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    // Good practice to let people know who's accessing their servers. See https://en.wikipedia.org/wiki/User_agent
    curl_setopt($ch, CURLOPT_USERAGENT, 'Dragons of Mugloar Solution (asadzaheer1408@gmail.com)');

    //Set your auth headers
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json'
    ));

    // get stringified data/output. See CURLOPT_RETURNTRANSFER
    $data = curl_exec($ch);

    // get info about the request
    $info = curl_getinfo($ch);

    $json = json_decode($data);

    $weather = checkWeather($json->gameId);

    $logFile = fopen("gameLog.log", "a") or die("Unable to open file!");
    if ($logFile) {
        fwrite($logFile, "[" . date("c") . "] " . "Game Started \nGame ID : " . $json->gameId .
            "\n" . "Weather : " . $weather->code[0] . " : " . $weather->message[0] . "\n"
            . "knight : {\"name\":" . $json->knight->name . ",\"attack\":" . $json->knight->attack
            . ",\"armor:\"" . $json->knight->armor . ",\"agility\":" . $json->knight->agility
            . ",\"endurance\":" . $json->knight->endurance . "}");
    } else {
        die('unable to open gameLog.log file.');
    }
    fclose($logFile);

    echo json_encode(['data' => $json, 'weather' => $weather]);

    // close curl resource to free up system resources
    curl_close($ch);
    die;
}

function checkWeather($gameId) {
    //setup the request, you can also use CURLOPT_URL
    $ch = curl_init('http://www.dragonsofmugloar.com/weather/api/report/' . $gameId);

    // Returns the data/output as a string instead of raw data
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    // Good practice to let people know who's accessing their servers. See https://en.wikipedia.org/wiki/User_agent
    curl_setopt($ch, CURLOPT_USERAGENT, 'Dragons of Mugloar Solution (asadzaheer1408@gmail.com)');

    //Set your auth headers
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json'
    ));

    // get stringified data/output. See CURLOPT_RETURNTRANSFER
    $data = curl_exec($ch);
    $oXML = new SimpleXMLElement($data);

    return $oXML;
}

function solveBattle($gameId, $knight, $weather) {
    $maxPoints   = 20;
    $maxPerStat  = 10;
    $minPerStat  = 0;
    $minPerStat  = 0;
    $dragon      = [];
    $dragonStats = [
        'attack' => 'scaleThickness',
        'armor' => 'clawSharpness',
        'agility' => 'wingStrength',
        'endurance' => 'fireBreath'
    ];

    global $totalFights;
    global $wins;
    global $loses;
    global $winPer;
    global $stormFights;
    global $stormFightsPer;

    //    print_r($gameId);
    //    print_r($knight);
    //    print_r($weather);

    $pointsLeft = $maxPoints;
    unset($knight['name']);
    //    $i = 0;
    if ($weather['code'] === 'T E' || $weather['code'] === 'SRO'|| $weather['code'] === 'FUNDEFINEDG') {
        $dragon = [
            'scaleThickness' => 5,
            'clawSharpness' => 5,
            'wingStrength' => 5,
            'fireBreath' => 5
        ];
    } else if ($weather['code'] === 'HVA') {
        $dragon = [
            'scaleThickness' => 5,
            'clawSharpness' => 10,
            'wingStrength' => 5,
            'fireBreath' => 0
        ];
    } else if ($weather['code'] === 'NMR') {
        $maxStatVal = max($knight);
        $maxStatKey = array_search($maxStatVal, $knight);

        foreach ($knight as $stat => $val) {
            if ($val < $minPerStat) {
                $points = $minPerStat;
            } else if ($pointsLeft >= $val) {
                //                if ($weather['code'] == 'NMR') {
                //                    if ($stat == 'endurance' && $val >= 8) {
                //                        $points = ($val + 2 <= 10) ? $val + 2 : 10;
                //                    } else if ($stat == 'attack' && $val > 6) {
                //                        $points = ($val + 2 <= 10) ? $val + 2 : 10;
                //                    } else if ($stat == 'armor' && $val >= 8) {
                //                        $points = ($val + 2 <= 10) ? $val + 2 : 10;
                //                    } else {
                //                        $points = $val;
                //                    }
                //                } else {
                $points = $val;
                //                }
            } else {
                $points = $pointsLeft;
            }
            $dragon[$dragonStats[$stat]] = (integer)$points;
            $pointsLeft                  -= $points;
        }
        $dragon[$dragonStats[$maxStatKey]] += 2;
        $pointsLeft                        -= 2;
        //                        print_r($pointsLeft);
        //                        print_r($dragon);
        while ($pointsLeft !== 0) {
            //            print_r($pointsLeft);
            //        if ($weather['code'] == 'NMR') {
            if ($knight['endurance'] >= 8) {
                if ($dragon['clawSharpness'] > $knight['armor']) {
                    $dragon['clawSharpness'] = ($dragon['clawSharpness'] - 2 < 0) ? 0 : $dragon['clawSharpness'] - 2;
                    $pointsLeft              += 2;
                } else if ($dragon['clawSharpness'] == $knight['armor']) {
                    $dragon['clawSharpness'] = ($dragon['clawSharpness'] - 1 < 0) ? 0 : $dragon['clawSharpness'] - 1;
                    $pointsLeft              += 1;
                } else {
                    $dragon['wingStrength'] = ($dragon['wingStrength'] - 1 < 0) ? 0 : $dragon['wingStrength'] - 1;
                    $pointsLeft             += 1;
                }
            } else if ($dragon['fireBreath'] == $knight['endurance']) {
                $dragon['fireBreath'] = ($dragon['fireBreath'] - 1 < 0) ? 0 : $dragon['fireBreath'] - 1;
                $pointsLeft           += 1;
            } else if ($dragon['wingStrength'] == $knight['agility']) {
                $dragon['wingStrength'] = ($dragon['wingStrength'] - 1 < 0) ? 0 : $dragon['wingStrength'] - 1;
                $pointsLeft             += 1;
            } else if ($dragon['clawSharpness'] == $knight['armor']) {
                $dragon['clawSharpness'] = ($dragon['clawSharpness'] - 1 < 0) ? 0 : $dragon['clawSharpness'] - 1;
                $pointsLeft              += 1;
            }
            //        }
            //            print_r($pointsLeft);
        }
    }
    //    print_r($pointsLeft);
    //        print_r($dragon);
    $json = array(
        'dragon' => $dragon
    );
    //    print_r($json);
    //    die;
    $url = 'http://www.dragonsofmugloar.com/api/game/' . $gameId . '/solution';

    //    $tmp            = $dragon;
    //    $json['dragon'] = array_map(function ($val) {
    //        settype($val, 'integer');
    //        //        print_r($val);
    //        //        print_r(gettype($val));
    //        return $val;
    //    }, $tmp['dragon']);
    //    print_r($json['dragon']);
    //    print_r(gettype($json['dragon']['clawSharpness']));
    //    print_r(json_encode($json));
    //    die;

    $headers = array(
        "Content-Type: application/json; charset=utf-8",
        "Content-Length: " . strlen(json_encode($json))
    );

    $channel = curl_init($url);
    curl_setopt($channel, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($channel, CURLOPT_CUSTOMREQUEST, "PUT");
    curl_setopt($channel, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($channel, CURLOPT_POSTFIELDS, json_encode($json));
    curl_setopt($channel, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($channel, CURLOPT_CONNECTTIMEOUT, 10);

    $data = curl_exec($channel);
    $info = curl_getinfo($channel);

    $data = json_decode($data);

    $totalFights += 1;
    if ($data->status == 'Victory') {
        $wins += 1;
    } else if ($weather['code'] == 'SRO') {
        $stormFights += 1;
    } else {
        $loses += 1;
    }
    $winPer         = ($wins / $totalFights) * 100;
    $losesPer       = ($loses / $totalFights) * 100;
    $stormFightsPer = ($stormFights / $totalFights) * 100;

    $fightData = [
        'totalFights' => $totalFights,
        'wins' => $wins,
        'winPer' => $winPer,
        'loses' => $loses,
        'losesPer' => $losesPer,
        'stormFights' => $stormFights,
        'stormFightsPer' => $stormFightsPer,
    ];

    $logFile = fopen("gameLog.log", "a") or die("Unable to open file!");
    if ($logFile) {
        fwrite($logFile, "[" . date("c") . "] " . "\nDragon :{\"fireBreath\":" . $dragon['fireBreath']
            . ",\"wingStrength\":" . $dragon["wingStrength"] . ",\"clawSharpness:\"" . $dragon['clawSharpness']
            . ",\"scaleThickness\":" . $dragon['scaleThickness'] . "}\nFight status : " . $data->status
            . " Fight message : " . $data->message . "\nTotal fights completed: " . $totalFights . " with win rate: "
            . $winPer . "% (" . $wins . " wins / " . $loses . " loses) out of which: " . $stormFightsPer . "% ("
            . $stormFights . " storms) were UNWINNABLE storms.");
    } else {
        die('unable to open gameLog.log file.');
    }
    fclose($logFile);

    echo json_encode(['data' => $data, 'requestInfo' => $info, 'dragon' => $json['dragon'], 'fightData' => $fightData]);
    curl_close($channel);

    die;
}